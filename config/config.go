package config

import (
	"github.com/spf13/viper"
	"log"
)

type DataBase struct {
	Host     string `mapstructure:"host" yaml:"host"`
	Port     int    `mapstructure:"port" yaml:"port"`
	User     string `mapstructure:"user" yaml:"user"`
	Password string `mapstructure:"password" yaml:"password"`
	Name     string `mapstructure:"name" yaml:"name"`
}

type JWT struct {
	SecretKey string `mapstructure:"secret_key" yaml:"secret_key"`
}

type Mail struct {
	Email    string `mapstructure:"email" yaml:"email"`
	Password string `mapstructure:"password" yaml:"password"`
}

type Server struct {
	Address string `mapstructure:"address" yaml:"address"`
	Port    string `mapstructure:"port" yaml:"port"`
	Swagger int    `mapstructure:"swagger" yaml:"swagger"`
}

type Config struct {
	DataBase DataBase `mapstructure:"database" yaml:"database"`
	JWT      JWT      `mapstructure:"jwt" yaml:"jwt"`
	Mail     Mail     `mapstructure:"mail" yaml:"mail"`
	Server   Server   `mapstructure:"server" yaml:"server"`
}

func LoadConfig() *Config {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		log.Panicln(err)
	}

	var cfg Config
	err = viper.Unmarshal(&cfg)
	if err != nil {
		log.Panicln(err)
	}
	return &cfg
}
