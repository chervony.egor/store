package config

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

func GenerateFromEnv() {
	err := godotenv.Load()
	if err != nil {
		fmt.Println("Error loading .env file:", err)
		os.Exit(1)
	}

	config := Config{}

	config.DataBase.Host = os.Getenv("DATABASE_HOST")
	config.DataBase.Port, _ = strconv.Atoi(os.Getenv("DATABASE_PORT"))
	config.DataBase.User = os.Getenv("DATABASE_USER")
	config.DataBase.Password = os.Getenv("DATABASE_PASSWORD")
	config.DataBase.Name = os.Getenv("DATABASE_NAME")

	config.JWT.SecretKey = os.Getenv("JWT_SECRET_KEY")

	config.Mail.Email = os.Getenv("MAIL_EMAIL")
	config.Mail.Password = os.Getenv("MAIL_PASSWORD")

	config.Server.Address = os.Getenv("SERVER_ADDRESS")
	config.Server.Port = os.Getenv("SERVER_PORT")
	config.Server.Swagger, _ = strconv.Atoi(os.Getenv("SERVER_SWAGGER"))

	data, err := yaml.Marshal(&config)
	if err != nil {
		fmt.Println("Error marshalling YAML:", err)
		os.Exit(1)
	}

	err = os.WriteFile("config.yaml", data, 0644)
	if err != nil {
		fmt.Println("Error writing config.yaml file:", err)
		os.Exit(1)
	}

	fmt.Println("config.yaml file created successfully")
}
