package main

import (
	"store/cmd/server"
	_ "store/docs"
)

// @title Store App Swagger
// @version 1.0
// @description  Swagger for Store App.
// @securityDefinitions.apikey Authorization
// @in header
// @name Authorization
func main() {
	server.Start()
}
