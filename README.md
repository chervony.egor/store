# Practical task (Store API Project)

This is a project for building a Store API using Go (Echo framework), PostgreSQL, JWT and Docker using clean architecture. <br>
The API provides endpoints for managing: products, categories, carts, wishes, orders, users and reviews.

## Project Structure

- 'app': Contains the main application code.
  - 'handlers': HTTP request handlers for different resources.
  - 'models': Structs representing different entities in the database.
  - 'repositories': Data access layer interfaces and implementations.
  - 'routes': HTTP route definitions.
  - 'services': Business logic layer for different resources.
- 'cmd/server': Main server application entry point.
- 'config': Configuration related code.
- 'db': Database initialization and connection code.
- 'extensions': Additional functionalities like JWT handling, email, and security.
- 'migrations': Database schema migration files.
- 'images': Contains the database UML diagram.

## Database Structure
![Database Structure](/images/DB_UML.png)

## Setup
### Manual
```shell
git clone https://gitlab.com/chervony.egor/store
cd store
go mod download
```
Then change `config/config.yaml` with yours credentials
```yaml
database:                         # Config for PostgreSQL database server
  host: postgresql                  # The IP address of the database server
  port: 5432                        # The port number of the database server
  user: User                        # The username for accessing the database
  password: UserPassword123         # The password for accessing the database
  name: Store                       # The name of the database

jwt:
  secret_key: supersecretfrase      # Secret phrase for JWT signature

mail:
  email: main@mail.com              # Mail
  password: qwe                     # Password for mail

server:
  address: 0.0.0.0                  # Server host address
  port: 8888                        # Server port
  swagger: 0                        # (0/1 | Disable/Enable) swagger in project
```
Then run project
```shell
go run
```

### Docker Compose
Start Docker on your pc
```shell
git clone https://gitlab.com/chervony.egor/store
cd store
docker-compose up --build -d
```
# Swagger
#### Swagger url `/swagger/index.html`

# Swagger end points

| Cart                                         | Category                                    |
|----------------------------------------------|---------------------------------------------|
| GET `/allCarts` - Get all carts              | GET `/allCategories` - Get all categories   |
| DELETE `/deleteCart` - Delete cart           | DELETE `/deleteCategory` - Delete category  |
| GET `/getCartById` - Get cart by id          | GET `/getCategoryById` - Get category by id |
| GET `/getCartByUserId` - Get cart by user id | POST `/newCategory` - Create a new category |
| POST `/newCart` - Create a new cart          | PUT `/updateCategory` -  Update category    |
| PUT `/updateCart` - Update cart              |  

| OrderProduct                                         | Order                                 | 
|------------------------------------------------------|---------------------------------------|
| GET `/allOrderProducts` - Get all order_product      | GET `/allOrders` - Get all orders     |
| DELETE `/deleteOrderProduct` - Delete order_product  | DELETE `/deleteOrder` - Delete order  |
| GET `/getOrderProductById` - Get order_product by id | GET `/getOrderById` - Get order by id |
| POST `/newOrderProduct` - Create a new order_product | POST `/newOrder` - Create a new order |
| PUT `/updateOrderProduct` - Update order_product     | PUT `/updateOrder` - Update order     |

| ProductImage                                          | Product                                                                  |
|-------------------------------------------------------|--------------------------------------------------------------------------|
| GET `/allProductImages` - Get all product_images      | GET `/allProducts` - Get all products                                    |
| DELETE `/deleteProductImage` - Delete product_image   | GET `/bestSelling` - Get best selling product                            |
| GET `/getProductImagesById` - Get product_image by id | GET `/bestSellingByCategoryId` - Get best selling product by category id |
| POST `/newProductImage` - Create a new product_image  | DELETE `/deleteProduct` - Delete product                                 |
| PUT `/updateProductImage` - Update product_images     | GET `/getByCategoryId` - Get product by category id                      |
|                                                       | GET `/getByCategoryName` - Get product by category name                  |
|                                                       | GET `/getProductById` - Get product by id                                |
|                                                       | POST `/newProduct` - Create a new product                                |
|                                                       | PUT `/updateProduct` - Update product                                    |

| Review                                                 | Shiping                                   |
|--------------------------------------------------------|-------------------------------------------|
| GET `/allReviews` - Get all reviews                    | GET `/allShipings` - Get all shipings     |
| DELETE `/deleteReview` - Delete review                 | DELETE `/deleteShiping` - Delete shiping  |
| GET `/getReviewById` - Get review by id                | GET `/getShipingById` - Get shiping by id |
| GET `/getReviewByProductId` - Get review by product id | POST `/newShiping` - Create a new shiping |
| POST `/newReview Create` - a new review                | PUT `/updateShiping` - Update shiping     |
| PUT `/updateReview` - Update review                    |                                           |

| User                                    | Wish                                     |
|-----------------------------------------|------------------------------------------|
| GET `/allUsers` - Get all users         | GET `/allWishs` - Get all wishs          |
| DELETE `/deleteUser` - Delete user      | DELETE `/deleteWish` - Delete wish       |
| GET `/forgotPassword` - Forgot password | GET `/getByUserId` - Get wish by user id |
| GET `/getUserById` - Get user by id     | GET `/getWishById` - Get wish by id      |
| GET `/login` - Login                    | POST `/newWish` - Create a new wish      |
| POST `/newUser` - Create a new user     | PUT `/updateWish` - Update wish          |
| POST `/resetPassword` - Reset password  |                                          |
| PUT `/updateUser` - Update user         |                                          |


















