CREATE TABLE reviews (
    id SERIAL PRIMARY KEY,
    user_id INT NOT NULL,
    product_id INT NOT NULL,
    rate INT NOT NULL,
    message TEXT NOT NULL,
    date DATE NOT NULL
);