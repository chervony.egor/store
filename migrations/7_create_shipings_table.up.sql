CREATE TABLE shipings (
     id SERIAL PRIMARY KEY,
     user_id INT NOT NULL,
     email TEXT NOT NULL,
     first_name TEXT NOT NULL,
     last_name TEXT NOT NULL,
     country TEXT NOT NULL,
     city TEXT NOT NULL,
     street TEXT NOT NULL,
     zip INT NOT NULL,
     phone TEXT NOT NULL
);