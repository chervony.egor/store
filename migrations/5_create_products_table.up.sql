CREATE TABLE products (
    id SERIAL PRIMARY KEY,
    brand TEXT NOT NULL,
    model TEXT NOT NULL,
    category_id INT NOT NULL,
    description TEXT NOT NULL,
    price BIGINT NOT NULL,
    sales BIGINT NOT NULL,
    avg_rate FLOAT NOT NULL,
    reviews BIGINT NOT NULL,
    quantity BIGINT NOT NULL
);