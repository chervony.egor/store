CREATE TABLE orders (
    id SERIAL PRIMARY KEY,
    shipping_id INT NOT NULL,
    user_id INT NOT NULL,
    date DATE NOT NULL
);