package swagger_structs

type IdStruct struct {
	ID int `json:"id,omitempty"`
}

// CART
type CreateCart struct {
	UserId    int   `json:"user_id"`
	ProductId int   `json:"product_id"`
	Quantity  int64 `json:"quantity"`
}

// CATEGORY
type CreateCategory struct {
	Name string `json:"name"`
}

// ORDER
type CreateOrder struct {
	ShipingId int    `json:"shipping_id"`
	UserId    int    `json:"user_id"`
	Date      string `json:"date"`
}

// ORDER_PRODUCT
type CreateOrderProduct struct {
	OrderId   int   `json:"order_id"`
	ProductId int   `json:"product_id"`
	Quantity  int64 `json:"quantity"`
}

// PRODUCT
type CreateProduct struct {
	Brand       string  `json:"brand"`
	Model       string  `json:"model"`
	CategoryId  int     `json:"category_id"`
	Description string  `json:"description"`
	Price       int64   `json:"price"`
	Sales       int64   `json:"sales"`
	AvgRate     float32 `json:"avg_rate"`
	Reviews     int64   `json:"reviews"`
	Quantity    int64   `json:"quantity"`
}

// PRODUCT IMAGE
type CreateProductImage struct {
	ProductId int    `json:"product_id"`
	ImagePath string `json:"image_path"`
}

// REVIEW
type CreateReview struct {
	UserId    int    `json:"user_id"`
	ProductId int    `json:"product_id"`
	Rate      int    `json:"rate"`
	Message   string `json:"message"`
	Date      string `json:"date"`
}

// SHIPING
type CreateShiping struct {
	UserId    int    `json:"user_id"`
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Country   string `json:"country"`
	City      string `json:"city"`
	Street    string `json:"street"`
	Zip       int    `json:"zip"`
	Phone     string `json:"phone"`
}

// USER
type CreateUser struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ResetPassword struct {
	Password string `json:"password"`
}

// WISH
type CreateWish struct {
	UserId    int `json:"user_id"`
	ProductId int `json:"product_id"`
}

//CREATE
// @Summary Create a new category
// @Tags Category
// @Description Create a new category
// @Param Cart body swagger_structs.CreateCategory true "Category object to be created"
// @Router /newCategory [post]

//GET BY ID
// @Summary Get category by id
// @Tags Category
// @Description Get category by id
// @Param id query int true "Id of category"
// @Router /getCategoryById [get]

//GET ALL
// @Summary Get all categories
// @Tags Category
// @Description Get all categories
// @Router /allCategories [get]

//UPDATE
// @Summary Update category
// @Tags Category
// @Description Update category
// @Param Cart body models.Category true "Category object to be updated"
// @Router /updateCategory [put]

//DELETE
// @Summary Delete category
// @Tags Category
// @Description Delete category
// @Param id query int true "Id of category to be deleted"
// @Router /deleteCategory [delete]

//GET OBJ BY SMTH ID
// @Summary Get cart by user id
// @Tags Cart
// @Description Get cart by user id
// @Param id query int true "Id of user to get cart"
// @Router /getCartByUserId [get]
