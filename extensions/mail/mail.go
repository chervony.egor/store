package mail

import (
	"net/smtp"
	"store/config"
)

func SendMail(to string, subject string, message string) {
	cfg := config.LoadConfig()
	from := cfg.Mail.Email
	password := cfg.Mail.Password

	auth := smtp.PlainAuth("", from, password, "smtp.gmail.com")

	msg := []byte("To: " + to + "\r\n" +
		"Subject: " + subject + "\r\n" +
		"\r\n" +
		message)

	err := smtp.SendMail("smtp.gmail.com:587", auth, from, []string{to}, msg)
	if err != nil {
		return
	}

}
