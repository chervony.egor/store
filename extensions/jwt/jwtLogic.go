package jwt

import (
	"fmt"
	"github.com/golang-jwt/jwt"
	"log"
	"store/app/models"
	"store/config"
	"time"
)

func GenerateToken(user *models.User) (string, error) {
	claims := &JWTClaims{
		ID: user.ID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: jwt.TimeFunc().Add(time.Hour * 744).Unix(),
		},
	}

	cfg := config.LoadConfig()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte(cfg.JWT.SecretKey))
	if err != nil {
		log.Printf("[TOKEN] %s", err)
	}

	return tokenString, nil
}
func GenerateResetToken(user *models.User) (string, error) {
	claims := &JWTClaims{
		ID: user.ID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: jwt.TimeFunc().Add(time.Hour).Unix(),
		},
	}

	cfg := config.LoadConfig()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte(cfg.JWT.SecretKey))
	if err != nil {
		log.Printf("[TOKEN] Error generating token: %v", err)
		return "", err
	}

	return tokenString, nil
}

func ValidateToken(tokenString string) (*JWTClaims, error) {
	cfg := config.LoadConfig()

	token, err := jwt.ParseWithClaims(tokenString, &JWTClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(cfg.JWT.SecretKey), nil
	})
	if err != nil {
		return nil, err
	}
	if !token.Valid {
		return nil, fmt.Errorf("Invalid token")
	}

	claims, ok := token.Claims.(*JWTClaims)
	if !ok {
		return nil, fmt.Errorf("Invalid token claims")
	}

	if time.Now().Unix() > claims.ExpiresAt {
		return nil, fmt.Errorf("Token has expired")
	}

	return claims, nil
}
