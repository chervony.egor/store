package jwt

import (
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"net/http"
	"store/config"
	"strings"
	"time"
)

func JwtMiddleware(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		tokenString := c.Request().Header.Get("Authorization")
		if tokenString == "" {
			return c.String(http.StatusUnauthorized, "Missing JWT token")
		}

		token, err := jwt.ParseWithClaims(strings.Split(tokenString, " ")[1], &JWTClaims{}, func(token *jwt.Token) (interface{}, error) {
			cfg := config.LoadConfig()
			return []byte(cfg.JWT.SecretKey), nil
		})
		if err != nil {
			return c.JSON(http.StatusUnauthorized, "Invalid JWT token")
		}
		if !token.Valid {
			return c.JSON(http.StatusUnauthorized, "Invalid JWT token")
		}

		claims, ok := token.Claims.(*JWTClaims)
		if !ok {
			return c.JSON(http.StatusUnauthorized, "Invalid JWT token claims")
		}

		if time.Now().Unix() > claims.ExpiresAt {
			return c.JSON(http.StatusUnauthorized, "Token has expired")
		}

		if err != nil || !token.Valid {
			return c.String(http.StatusUnauthorized, "Invalid or expired JWT token")
		}

		return handlerFunc(c)
	}
}
