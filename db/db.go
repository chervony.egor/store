package db

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
	"os/exec"
	"store/config"
	"strconv"
)

var db *sqlx.DB

func init() {
	cfg := config.LoadConfig()

	connectionString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.DataBase.Host, cfg.DataBase.Port, cfg.DataBase.User, cfg.DataBase.Password, cfg.DataBase.Name)

	database, err := sqlx.Connect("postgres", connectionString)
	if err != nil {
		log.Fatalf("[DB] failed to connect to database: %v", err)
	}
	db = database

}

func GetDb() *sqlx.DB {
	return db
}

func AutoMigrate() {
	cfg := config.LoadConfig()
	cmd := exec.Command("go", "install", "-tags", "postgres", "github.com/golang-migrate/migrate/v4/cmd/migrate@latest")
	err := cmd.Run()
	if err != nil {
		log.Printf("[COMMAND] %s", err)
	}
	cmd = exec.Command("migrate", "-path", "migrations", "-database", "postgres://"+cfg.DataBase.User+":"+cfg.DataBase.Password+"@"+cfg.DataBase.Host+":"+strconv.FormatInt(int64(cfg.DataBase.Port), 10)+"/"+cfg.DataBase.Name+"?sslmode=disable", "up")
	err = cmd.Run()
	if err != nil {
		log.Printf("[COMMAND] %s", err)
	}
}
