package models

type Review struct {
	ID        int    `json:"id,omitempty" db:"id,omitempty"`
	UserId    int    `json:"user_id" db:"user_id"`
	ProductId int    `json:"product_id" db:"product_id"`
	Rate      int    `json:"rate" db:"rate"`
	Message   string `json:"message" db:"message"`
	Date      string `json:"date" db:"date"`
}
