package models

type Shiping struct {
	ID        int    `json:"id,omitempty" db:"id,omitempty"`
	UserId    int    `json:"user_id" db:"user_id"`
	Email     string `json:"email" db:"email"`
	FirstName string `json:"first_name" db:"first_name"`
	LastName  string `json:"last_name" db:"last_name"`
	Country   string `json:"country" db:"country"`
	City      string `json:"city" db:"city"`
	Street    string `json:"street" db:"street"`
	Zip       int    `json:"zip" db:"zip"`
	Phone     string `json:"phone" db:"phone"`
}
