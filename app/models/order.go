package models

type Order struct {
	ID        int    `json:"id,omitempty" db:"id,omitempty"`
	ShipingId int    `json:"shipping_id" db:"shipping_id"`
	UserId    int    `json:"user_id" db:"user_id"`
	Date      string `json:"date" db:"date"`
}
