package models

type Category struct {
	ID   int    `json:"id,omitempty" db:"id,omitempty"`
	Name string `json:"name" db:"name"`
}
