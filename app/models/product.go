package models

type Product struct {
	ID          int     `json:"id,omitempty" db:"id,omitempty"`
	Brand       string  `json:"brand" db:"brand"`
	Model       string  `json:"model" db:"model"`
	CategoryId  int     `json:"category_id" db:"category_id"`
	Description string  `json:"description" db:"description"`
	Price       int64   `json:"price" db:"price"`
	Sales       int64   `json:"sales" db:"sales"`
	AvgRate     float32 `json:"avg_rate" db:"avg_rate"`
	Reviews     int64   `json:"reviews" db:"reviews"`
	Quantity    int64   `json:"quantity" db:"quantity"`
}
