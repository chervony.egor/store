package models

type OrderProduct struct {
	ID        int   `json:"id,omitempty" db:"id,omitempty"`
	OrderId   int   `json:"order_id" db:"order_id"`
	ProductId int   `json:"product_id" db:"product_id"`
	Quantity  int64 `json:"quantity" db:"quantity"`
}
