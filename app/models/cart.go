package models

type Cart struct {
	ID        int   `json:"id,omitempty" db:"id,omitempty"`
	UserId    int   `json:"user_id" db:"user_id"`
	ProductId int   `json:"product_id" db:"product_id"`
	Quantity  int64 `json:"quantity" db:"quantity"`
}
