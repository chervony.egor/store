package models

type User struct {
	ID       int    `json:"id,omitempty" db:"id,omitempty"`
	Email    string `json:"email" db:"email"`
	Password string `json:"password" db:"password"`
}
