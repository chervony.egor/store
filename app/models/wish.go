package models

type Wish struct {
	ID        int `json:"id,omitempty" db:"id,omitempty"`
	UserId    int `json:"user_id" db:"user_id"`
	ProductId int `json:"product_id" db:"product_id"`
}
