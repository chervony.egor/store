package models

type ProductImage struct {
	ID        int    `json:"id,omitempty" db:"id,omitempty"`
	ProductId int    `json:"product_id" db:"product_id"`
	ImagePath string `json:"image_path" db:"image_path"`
}
