package routes

import (
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	echoSwagger "github.com/swaggo/echo-swagger"
	"store/app/handlers/init_handlers"
	"store/config"
	"store/extensions/jwt"
)

func InitRoutes(e *echo.Echo, db *sqlx.DB) {
	init_handlers.InitHandlers(db)
	allHandlers := init_handlers.GetHandlers()
	//SWAGGER
	cfg := config.LoadConfig()
	if cfg.Server.Swagger == 1 {
		e.GET("/swagger/*", echoSwagger.WrapHandler)
	}
	//CART
	e.POST("/newCart", allHandlers.CartHandler.CreateCart)
	e.GET("/allCarts", allHandlers.CartHandler.GetAllCarts)
	e.GET("/getCartById", allHandlers.CartHandler.GetCartByID)
	e.PUT("/updateCart", allHandlers.CartHandler.UpdateCart, jwt.JwtMiddleware)
	e.DELETE("/deleteCart", allHandlers.CartHandler.DeleteCart, jwt.JwtMiddleware)
	e.GET("/getCartByUserId", allHandlers.CartHandler.GetCartByUserId)
	//CATEGORY
	e.POST("/newCategory", allHandlers.CategoryHandler.CreateCategory)
	e.GET("/allCategories", allHandlers.CategoryHandler.GetAllCategories)
	e.GET("/getCategoryById", allHandlers.CategoryHandler.GetCategoryByID)
	e.PUT("/updateCategory", allHandlers.CategoryHandler.UpdateCategory, jwt.JwtMiddleware)
	e.DELETE("/deleteCategory", allHandlers.CategoryHandler.DeleteCategory, jwt.JwtMiddleware)
	//ORDER
	e.POST("/newOrder", allHandlers.OrderHandler.CreateOrder)
	e.GET("/allOrders", allHandlers.OrderHandler.GetAllOrders)
	e.GET("/getOrderById", allHandlers.OrderHandler.GetOrderByID)
	e.PUT("/updateOrder", allHandlers.OrderHandler.UpdateOrder, jwt.JwtMiddleware)
	e.DELETE("/deleteOrder", allHandlers.OrderHandler.DeleteOrder, jwt.JwtMiddleware)
	//ORDER_PRODUCT
	e.POST("/newOrderProduct", allHandlers.OrderProductHandler.CreateOrderProduct)
	e.GET("/allOrderProducts", allHandlers.OrderProductHandler.GetAllOrderProducts)
	e.GET("/getOrderProductById", allHandlers.OrderProductHandler.GetOrderProductByID)
	e.PUT("/updateOrderProduct", allHandlers.OrderProductHandler.UpdateOrderProduct, jwt.JwtMiddleware)
	e.DELETE("/deleteOrderProduct", allHandlers.OrderProductHandler.DeleteOrderProduct, jwt.JwtMiddleware)
	//PRODUCT
	e.POST("/newProduct", allHandlers.ProductHandler.CreateProduct)
	e.GET("/allProducts", allHandlers.ProductHandler.GetAllProducts)
	e.GET("/getProductById", allHandlers.ProductHandler.GetProductByID)
	e.PUT("/updateProduct", allHandlers.ProductHandler.UpdateProduct, jwt.JwtMiddleware)
	e.DELETE("/deleteProduct", allHandlers.ProductHandler.DeleteProduct, jwt.JwtMiddleware)
	e.GET("/bestSelling", allHandlers.ProductHandler.BestSellingProduct)
	e.GET("/getByCategoryId", allHandlers.ProductHandler.GetProductByCategoryId)
	e.GET("/getByCategoryName", allHandlers.ProductHandler.GetProductByCategoryName)
	e.GET("/bestSellingByCategoryId", allHandlers.ProductHandler.BestSellingByCategoryId)
	//PRODUCT IMAGE
	e.POST("/newProductImage", allHandlers.ProductImageHandler.CreateProductImage)
	e.GET("/allProductImages", allHandlers.ProductImageHandler.GetAllProductImages)
	e.GET("/getProductImagesById", allHandlers.ProductImageHandler.GetProductImageByID)
	e.PUT("/updateProductImage", allHandlers.ProductImageHandler.UpdateProductImage, jwt.JwtMiddleware)
	e.DELETE("/deleteProductImage", allHandlers.ProductImageHandler.DeleteProductImage, jwt.JwtMiddleware)
	//REVIEW
	e.POST("/newReview", allHandlers.ReviewHandler.CreateReview)
	e.GET("/allReviews", allHandlers.ReviewHandler.GetAllReviews)
	e.GET("/getReviewById", allHandlers.ReviewHandler.GetReviewByID)
	e.PUT("/updateReview", allHandlers.ReviewHandler.UpdateReview, jwt.JwtMiddleware)
	e.DELETE("/deleteReview", allHandlers.ReviewHandler.DeleteReview, jwt.JwtMiddleware)
	e.GET("/getReviewByProductId", allHandlers.ReviewHandler.GetReviewByProductId)
	//SHIPING
	e.POST("/newShiping", allHandlers.ShipingHandler.CreateShiping)
	e.GET("/allShipings", allHandlers.ShipingHandler.GetAllShipings)
	e.GET("/getShipingById", allHandlers.ShipingHandler.GetShipingByID)
	e.PUT("/updateShiping", allHandlers.ShipingHandler.UpdateShiping, jwt.JwtMiddleware)
	e.DELETE("/deleteShiping", allHandlers.ShipingHandler.DeleteShiping, jwt.JwtMiddleware)
	//USER
	e.POST("/newUser", allHandlers.UserHandler.CreateUser)
	e.GET("/allUsers", allHandlers.UserHandler.GetAllUsers)
	e.GET("/getUserById", allHandlers.UserHandler.GetUserByID)
	e.PUT("/updateUser", allHandlers.UserHandler.UpdateUser, jwt.JwtMiddleware)
	e.DELETE("/deleteUser", allHandlers.UserHandler.DeleteUser, jwt.JwtMiddleware)
	e.GET("/login", allHandlers.UserHandler.Login)
	e.GET("/forgotPassword", allHandlers.UserHandler.ForgotPassword)
	e.POST("/resetPassword", allHandlers.UserHandler.ResetPassword, jwt.JwtMiddleware)
	//WISH
	e.POST("/newWish", allHandlers.WishHandler.CreateWish)
	e.GET("/allWishs", allHandlers.WishHandler.GetAllWishs)
	e.GET("/getWishById", allHandlers.WishHandler.GetWishByID)
	e.PUT("/updateWish", allHandlers.WishHandler.UpdateWish, jwt.JwtMiddleware)
	e.DELETE("/deleteWish", allHandlers.WishHandler.DeleteWish, jwt.JwtMiddleware)
	e.GET("/getByUserId", allHandlers.WishHandler.GetWishByUserID)
}
