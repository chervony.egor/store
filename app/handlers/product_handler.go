package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"store/app/models"
	"store/app/services"
	"strconv"
)

type ProductHandler struct {
	ProductService *services.ProductService
}

func NewProductHandler(ProductService *services.ProductService) *ProductHandler {
	return &ProductHandler{ProductService: ProductService}
}

// @Summary Create a new product
// @Tags Product
// @Description Create a new product
// @Param Product body swagger_structs.CreateProduct true "Product object to be created"
// @Router /newProduct [post]
func (h *ProductHandler) CreateProduct(c echo.Context) error {
	product := &models.Product{}
	if err := c.Bind(product); err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.ProductService.CreateProduct(product)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create product")
	}

	return c.JSON(http.StatusOK, product)
}

// @Summary Get product by id
// @Tags Product
// @Description Get product by id
// @Param id query int true "Id of product"
// @Router /getProductById [get]
func (h *ProductHandler) GetProductByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	product, err := h.ProductService.GetProductByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get product")
	}

	return c.JSON(http.StatusOK, product)
}

// @Summary Get all products
// @Tags Product
// @Description Get all products
// @Router /allProducts [get]
func (h *ProductHandler) GetAllProducts(c echo.Context) error {
	products, err := h.ProductService.GetAllProducts()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all products")
	}
	return c.JSON(http.StatusOK, products)
}

// @Summary Update product
// @Security Authorization
// @Tags Product
// @Description Update product
// @Param Product body models.Product true "Product object to be updated"
// @Router /updateProduct [put]
func (h *ProductHandler) UpdateProduct(c echo.Context) error {
	product := &models.Product{}
	if err := c.Bind(product); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.ProductService.UpdateProduct(product)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update product")
	}
	return c.JSON(http.StatusOK, "Product updated")
}

// @Summary Delete product
// @Security Authorization
// @Tags Product
// @Description Delete product
// @Param id query int true "Id of product to be deleted"
// @Router /deleteProduct [delete]
func (h *ProductHandler) DeleteProduct(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.ProductService.DeleteProduct(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete product")
	}
	return c.JSON(http.StatusOK, "Product deleted")
}

// @Summary Get best selling product
// @Tags Product
// @Description Get best selling product
// @Router /bestSelling [get]
func (h *ProductHandler) BestSellingProduct(c echo.Context) error {
	product, err := h.ProductService.BestSelling()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get best selling product")
	}
	return c.JSON(http.StatusOK, product)
}

// @Summary Get product by category id
// @Tags Product
// @Description Get product by category id
// @Param id query int true "Id of category"
// @Router /getByCategoryId [get]
func (h *ProductHandler) GetProductByCategoryId(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	products, err := h.ProductService.GetByCategoryId(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get product by category id")
	}
	return c.JSON(http.StatusOK, products)
}

// @Summary Get product by category name
// @Tags Product
// @Description Get product by category name
// @Param name query string true "Name of category"
// @Router /getByCategoryName [get]
func (h *ProductHandler) GetProductByCategoryName(c echo.Context) error {
	name := c.QueryParam("name")
	products, err := h.ProductService.GetByCategoryName(name)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get product by category name")
	}
	return c.JSON(http.StatusOK, products)
}

// @Summary Get best selling product by category id
// @Tags Product
// @Description Get best selling product by category id
// @Param id query int true "Id of category"
// @Router /bestSellingByCategoryId [get]
func (h *ProductHandler) BestSellingByCategoryId(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	products, err := h.ProductService.BestSellingByCategoryId(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get best selling product by category id")
	}
	return c.JSON(http.StatusOK, products)
}
