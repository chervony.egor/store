package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"store/app/models"
	"store/app/services"
	"strconv"
)

type OrderHandler struct {
	OrderService *services.OrderService
}

func NewOrderHandler(OrderService *services.OrderService) *OrderHandler {
	return &OrderHandler{OrderService: OrderService}
}

// @Summary Create a new order
// @Tags Order
// @Description Create a new order
// @Param Order body swagger_structs.CreateOrder true "Order object to be created"
// @Router /newOrder [post]
func (h *OrderHandler) CreateOrder(c echo.Context) error {
	order := &models.Order{}
	if err := c.Bind(order); err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.OrderService.CreateOrder(order)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create order")
	}

	return c.JSON(http.StatusOK, order)
}

// @Summary Get order by id
// @Tags Order
// @Description Get order by id
// @Param id query int true "Id of order"
// @Router /getOrderById [get]
func (h *OrderHandler) GetOrderByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	order, err := h.OrderService.GetOrderByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get order")
	}

	return c.JSON(http.StatusOK, order)
}

// @Summary Get all orders
// @Tags Order
// @Description Get all orders
// @Router /allOrders [get]
func (h *OrderHandler) GetAllOrders(c echo.Context) error {
	orders, err := h.OrderService.GetAllOrders()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all orders")
	}
	return c.JSON(http.StatusOK, orders)
}

// @Summary Update order
// @Security Authorization
// @Tags Order
// @Description Update order
// @Param Order body models.Order true "Order object to be updated"
// @Router /updateOrder [put]
func (h *OrderHandler) UpdateOrder(c echo.Context) error {
	order := &models.Order{}
	if err := c.Bind(order); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.OrderService.UpdateOrder(order)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update order")
	}
	return c.JSON(http.StatusOK, "Order updated")
}

// @Summary Delete order
// @Security Authorization
// @Tags Order
// @Description Delete order
// @Param id query int true "Id of order to be deleted"
// @Router /deleteOrder [delete]
func (h *OrderHandler) DeleteOrder(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.OrderService.DeleteOrder(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete order")
	}
	return c.JSON(http.StatusOK, "Order deleted")
}
