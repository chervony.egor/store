package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"store/app/models"
	"store/app/services"
	"strconv"
)

type WishHandler struct {
	WishService *services.WishService
}

func NewWishHandler(WishService *services.WishService) *WishHandler {
	return &WishHandler{WishService: WishService}
}

// @Summary Create a new wish
// @Tags Wish
// @Description Create a new wish
// @Param Wish body swagger_structs.CreateWish true "Wish object to be created"
// @Router /newWish [post]
func (h *WishHandler) CreateWish(c echo.Context) error {
	wish := &models.Wish{}
	if err := c.Bind(wish); err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.WishService.CreateWish(wish)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create wish")
	}

	return c.JSON(http.StatusOK, wish)
}

// @Summary Get wish by id
// @Tags Wish
// @Description Get wish by id
// @Param id query int true "Id of wish"
// @Router /getWishById [get]
func (h *WishHandler) GetWishByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	wish, err := h.WishService.GetWishByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get wish")
	}

	return c.JSON(http.StatusOK, wish)
}

// @Summary Get all wishs
// @Tags Wish
// @Description Get all wishs
// @Router /allWishs [get]
func (h *WishHandler) GetAllWishs(c echo.Context) error {
	wishs, err := h.WishService.GetAllWishs()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all wishes")
	}
	return c.JSON(http.StatusOK, wishs)
}

// @Summary Update wish
// @Security Authorization
// @Tags Wish
// @Description Update wish
// @Param Wish body models.Wish true "Wish object to be updated"
// @Router /updateWish [put]
func (h *WishHandler) UpdateWish(c echo.Context) error {
	wish := &models.Wish{}
	if err := c.Bind(wish); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.WishService.UpdateWish(wish)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update wish")
	}
	return c.JSON(http.StatusOK, "Wish updated")
}

// @Summary Delete wish
// @Security Authorization
// @Tags Wish
// @Description Delete wish
// @Param id query int true "Id of wish to be deleted"
// @Router /deleteWish [delete]
func (h *WishHandler) DeleteWish(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.WishService.DeleteWish(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete wish")
	}
	return c.JSON(http.StatusOK, "Wish deleted")
}

// @Summary Get wish by user id
// @Tags Wish
// @Description Get wish by user id
// @Param id query int true "Id of user to get wish"
// @Router /getByUserId [get]
func (h *WishHandler) GetWishByUserID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	wishes, err := h.WishService.GetByUserId(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get wish")
	}

	return c.JSON(http.StatusOK, wishes)
}
