package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"store/app/models"
	"store/app/services"
	"strconv"
)

type CategoryHandler struct {
	CategoryService *services.CategoryService
}

func NewCategoryHandler(CategoryService *services.CategoryService) *CategoryHandler {
	return &CategoryHandler{CategoryService: CategoryService}
}

// @Summary Create a new category
// @Tags Category
// @Description Create a new category
// @Param Category body swagger_structs.CreateCategory true "Category object to be created"
// @Router /newCategory [post]
func (h *CategoryHandler) CreateCategory(c echo.Context) error {
	category := &models.Category{}
	if err := c.Bind(category); err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.CategoryService.CreateCategory(category)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create category")
	}

	return c.JSON(http.StatusOK, category)
}

// @Summary Get category by id
// @Tags Category
// @Description Get category by id
// @Param id query int true "Id of category"
// @Router /getCategoryById [get]
func (h *CategoryHandler) GetCategoryByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	category, err := h.CategoryService.GetCategoryByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get category")
	}

	return c.JSON(http.StatusOK, category)
}

// @Summary Get all categories
// @Tags Category
// @Description Get all categories
// @Router /allCategories [get]
func (h *CategoryHandler) GetAllCategories(c echo.Context) error {
	categorys, err := h.CategoryService.GetAllCategories()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all categorys")
	}
	return c.JSON(http.StatusOK, categorys)
}

// @Summary Update category
// @Security Authorization
// @Tags Category
// @Description Update category
// @Param Category body models.Category true "Category object to be updated"
// @Router /updateCategory [put]
func (h *CategoryHandler) UpdateCategory(c echo.Context) error {
	category := &models.Category{}
	if err := c.Bind(category); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.CategoryService.UpdateCategory(category)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update category")
	}
	return c.JSON(http.StatusOK, "Category updated")
}

// @Summary Delete category
// @Security Authorization
// @Tags Category
// @Description Delete category
// @Param id query int true "Id of category to be deleted"
// @Router /deleteCategory [delete]
func (h *CategoryHandler) DeleteCategory(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.CategoryService.DeleteCategory(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete category")
	}
	return c.JSON(http.StatusOK, "Category deleted")
}
