package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"store/app/models"
	"store/app/services"
	"store/config"
	"store/extensions/jwt"
	"store/extensions/mail"
	"strconv"
	"strings"
)

type UserHandler struct {
	UserService *services.UserService
}

func NewUserHandler(UserService *services.UserService) *UserHandler {
	return &UserHandler{UserService: UserService}
}

// @Summary Create a new user
// @Tags User
// @Description Create a new user
// @Param User body swagger_structs.CreateUser true "User object to be created"
// @Router /newUser [post]
func (h *UserHandler) CreateUser(c echo.Context) error {
	user := &models.User{}
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.UserService.CreateUser(user)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create user")
	}

	return c.JSON(http.StatusOK, user)
}

// @Summary Get user by id
// @Tags User
// @Description Get user by id
// @Param id query int true "Id of user"
// @Router /getUserById [get]
func (h *UserHandler) GetUserByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	user, err := h.UserService.GetUserByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get user")
	}

	return c.JSON(http.StatusOK, user)
}

// @Summary Get all users
// @Tags User
// @Description Get all users
// @Router /allUsers [get]
func (h *UserHandler) GetAllUsers(c echo.Context) error {
	users, err := h.UserService.GetAllUsers()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all users")
	}
	return c.JSON(http.StatusOK, users)
}

// @Summary Update user
// @Security Authorization
// @Tags User
// @Description Update user
// @Param User body models.User true "User object to be updated"
// @Router /updateUser [put]
func (h *UserHandler) UpdateUser(c echo.Context) error {
	user := &models.User{}
	if err := c.Bind(user); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.UserService.UpdateUser(user)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update user")
	}
	return c.JSON(http.StatusOK, "User updated")
}

// @Summary Delete user
// @Security Authorization
// @Tags User
// @Description Delete user
// @Param id query int true "Id of user to be deleted"
// @Router /deleteUser [delete]
func (h *UserHandler) DeleteUser(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.UserService.DeleteUser(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete user")
	}
	return c.JSON(http.StatusOK, "User deleted")
}

// @Summary Forgot password
// @Tags User
// @Description Forgot password
// @Param email query string true "Email of user"
// @Router /forgotPassword [get]
func (h *UserHandler) ForgotPassword(c echo.Context) error {
	email := c.QueryParam("email")
	dbUser, err := h.UserService.GetByEmail(email)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, "Invalid user")
	}
	token, err := jwt.GenerateResetToken(dbUser)
	if err != nil {
		return err
	}
	cfg := config.LoadConfig()
	address := cfg.Server.Address + ":" + cfg.Server.Port
	link := address + "/resetPassword"
	go mail.SendMail(email, "Reset Password", link)
	return c.JSON(http.StatusOK, "Bearer "+token)
}

// @Summary Reset password
// @Security Authorization
// @Tags User
// @Description Reset password
// @Param ResetPassword body swagger_structs.ResetPassword true "New password for user"
// @Router /resetPassword [post]
func (h *UserHandler) ResetPassword(c echo.Context) error {
	resetToken := strings.Split(c.Request().Header.Get("Authorization"), " ")[1]

	claims, err := jwt.ValidateToken(resetToken)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, err)
	}
	if err != nil {
		return c.JSON(http.StatusInternalServerError, "User not found")
	}
	user := &models.User{}
	if err := c.Bind(user); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.UserService.UserRepository.UpdatePassword(claims.ID, user.Password)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update password")
	}
	return c.JSON(http.StatusOK, "Password updated")
}

// @Summary Login
// @Tags User
// @Description Login
// @Param email query string true "Email of user"
// @Param password query string true "Password of user"
// @Router /login [get]
func (h *UserHandler) Login(c echo.Context) error {
	email := c.QueryParam("email")
	password := c.QueryParam("password")
	requestUser := &models.User{
		Email:    email,
		Password: password,
	}
	token, err := h.UserService.Login(requestUser)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to login")
	}
	return c.JSON(http.StatusOK, "Bearer "+token)
}
