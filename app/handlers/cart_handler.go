package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"store/app/models"
	"store/app/services"
	"strconv"
)

type CartHandler struct {
	CartService *services.CartService
}

func NewCartHandler(CartService *services.CartService) *CartHandler {
	return &CartHandler{CartService: CartService}
}

// @Summary Create a new cart
// @Tags Cart
// @Description Create a new cart
// @Param Cart body swagger_structs.CreateCart true "Cart object to be created"
// @Router /newCart [post]
func (h *CartHandler) CreateCart(c echo.Context) error {
	cart := &models.Cart{}
	if err := c.Bind(cart); err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.CartService.CreateCart(cart)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create cart")
	}

	return c.JSON(http.StatusOK, cart)
}

// @Summary Get cart by id
// @Tags Cart
// @Description Get cart by id
// @Param id query int true "Id of cart"
// @Router /getCartById [get]
func (h *CartHandler) GetCartByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	cart, err := h.CartService.GetCartByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get cart")
	}

	return c.JSON(http.StatusOK, cart)
}

// @Summary Get all carts
// @Tags Cart
// @Description Get all carts
// @Router /allCarts [get]
func (h *CartHandler) GetAllCarts(c echo.Context) error {
	carts, err := h.CartService.GetAllCarts()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all carts")
	}
	return c.JSON(http.StatusOK, carts)
}

// @Summary Update cart
// @Security Authorization
// @Tags Cart
// @Description Update cart
// @Param Cart body models.Cart true "Cart object to be updated"
// @Router /updateCart [put]
func (h *CartHandler) UpdateCart(c echo.Context) error {
	cart := &models.Cart{}
	if err := c.Bind(cart); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.CartService.UpdateCart(cart)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update cart")
	}
	return c.JSON(http.StatusOK, "Cart updated")
}

// @Summary Delete cart
// @Security Authorization
// @Tags Cart
// @Description Delete cart
// @Param id query int true "Id of cart to be deleted"
// @Router /deleteCart [delete]
func (h *CartHandler) DeleteCart(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.CartService.DeleteCart(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete cart")
	}
	return c.JSON(http.StatusOK, "Cart deleted")
}

// @Summary Get cart by user id
// @Tags Cart
// @Description Get cart by user id
// @Param id query int true "Id of user to get cart"
// @Router /getCartByUserId [get]
func (h *CartHandler) GetCartByUserId(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	users, err := h.CartService.GetByUserId(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get carts")
	}
	return c.JSON(http.StatusOK, users)
}
