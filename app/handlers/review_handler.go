package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"store/app/models"
	"store/app/services"
	"strconv"
)

type ReviewHandler struct {
	ReviewService *services.ReviewService
}

func NewReviewHandler(ReviewService *services.ReviewService) *ReviewHandler {
	return &ReviewHandler{ReviewService: ReviewService}
}

// @Summary Create a new review
// @Tags Review
// @Description Create a new review
// @Param Review body swagger_structs.CreateReview true "Review object to be created"
// @Router /newReview [post]
func (h *ReviewHandler) CreateReview(c echo.Context) error {
	review := &models.Review{}
	if err := c.Bind(review); err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.ReviewService.CreateReview(review)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create review")
	}

	return c.JSON(http.StatusOK, review)
}

// @Summary Get review by id
// @Tags Review
// @Description Get review by id
// @Param id query int true "Id of review"
// @Router /getReviewById [get]
func (h *ReviewHandler) GetReviewByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	review, err := h.ReviewService.GetReviewByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get review")
	}

	return c.JSON(http.StatusOK, review)
}

// @Summary Get all reviews
// @Tags Review
// @Description Get all reviews
// @Router /allReviews [get]
func (h *ReviewHandler) GetAllReviews(c echo.Context) error {
	reviews, err := h.ReviewService.GetAllReviews()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all reviews")
	}
	return c.JSON(http.StatusOK, reviews)
}

// @Summary Update review
// @Security Authorization
// @Tags Review
// @Description Update review
// @Param Review body models.Review true "Review object to be updated"
// @Router /updateReview [put]
func (h *ReviewHandler) UpdateReview(c echo.Context) error {
	review := &models.Review{}
	if err := c.Bind(review); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.ReviewService.UpdateReview(review)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update review")
	}
	return c.JSON(http.StatusOK, "Review updated")
}

// @Summary Delete review
// @Security Authorization
// @Tags Review
// @Description Delete review
// @Param id query int true "Id of review to be deleted"
// @Router /deleteReview [delete]
func (h *ReviewHandler) DeleteReview(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.ReviewService.DeleteReview(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete review")
	}
	return c.JSON(http.StatusOK, "Review deleted")
}

// @Summary Get review by product id
// @Tags Review
// @Description Get review by product id
// @Param id query int true "Id of product to get review"
// @Router /getReviewByProductId [get]
func (h *ReviewHandler) GetReviewByProductId(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	reviews, err := h.ReviewService.GetByProductId(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get reviews")
	}
	return c.JSON(http.StatusOK, reviews)
}
