package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"store/app/models"
	"store/app/services"
	"strconv"
)

type ShipingHandler struct {
	ShipingService *services.ShipingService
}

func NewShipingHandler(ShipingService *services.ShipingService) *ShipingHandler {
	return &ShipingHandler{ShipingService: ShipingService}
}

// @Summary Create a new shiping
// @Tags Shiping
// @Description Create a new shiping
// @Param Shiping body swagger_structs.CreateShiping true "Shiping object to be created"
// @Router /newShiping [post]
func (h *ShipingHandler) CreateShiping(c echo.Context) error {
	shiping := &models.Shiping{}
	if err := c.Bind(shiping); err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.ShipingService.CreateShiping(shiping)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create shiping")
	}

	return c.JSON(http.StatusOK, shiping)
}

// @Summary Get shiping by id
// @Tags Shiping
// @Description Get shiping by id
// @Param id query int true "Id of shiping"
// @Router /getShipingById [get]
func (h *ShipingHandler) GetShipingByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	shiping, err := h.ShipingService.GetShipingByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get shiping")
	}

	return c.JSON(http.StatusOK, shiping)
}

// @Summary Get all shipings
// @Tags Shiping
// @Description Get all shipings
// @Router /allShipings [get]
func (h *ShipingHandler) GetAllShipings(c echo.Context) error {
	shipings, err := h.ShipingService.GetAllShipings()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all shipings")
	}
	return c.JSON(http.StatusOK, shipings)
}

// @Summary Update shiping
// @Security Authorization
// @Tags Shiping
// @Description Update shiping
// @Param Shiping body models.Shiping true "Shiping object to be updated"
// @Router /updateShiping [put]
func (h *ShipingHandler) UpdateShiping(c echo.Context) error {
	shiping := &models.Shiping{}
	if err := c.Bind(shiping); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.ShipingService.UpdateShiping(shiping)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update shiping")
	}
	return c.JSON(http.StatusOK, "Shiping updated")
}

// @Summary Delete shiping
// @Security Authorization
// @Tags Shiping
// @Description Delete shiping
// @Param id query int true "Id of shiping to be deleted"
// @Router /deleteShiping [delete]
func (h *ShipingHandler) DeleteShiping(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.ShipingService.DeleteShiping(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete shiping")
	}
	return c.JSON(http.StatusOK, "Shiping deleted")
}
