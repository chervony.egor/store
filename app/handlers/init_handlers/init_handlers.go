package init_handlers

import (
	"github.com/jmoiron/sqlx"
	"store/app/handlers"
	"store/app/repositories"
	"store/app/services"
)

type Handlers struct {
	CartHandler         *handlers.CartHandler
	CategoryHandler     *handlers.CategoryHandler
	OrderHandler        *handlers.OrderHandler
	OrderProductHandler *handlers.OrderProductHandler
	ProductHandler      *handlers.ProductHandler
	ProductImageHandler *handlers.ProductImageHandler
	ReviewHandler       *handlers.ReviewHandler
	ShipingHandler      *handlers.ShipingHandler
	UserHandler         *handlers.UserHandler
	WishHandler         *handlers.WishHandler
}

var handler *Handlers

func InitHandlers(db *sqlx.DB) {

	cartRepository := repositories.NewCartRepository(db)
	cartService := services.NewCartService(cartRepository)
	cartHandler := handlers.NewCartHandler(cartService)

	categoryRepository := repositories.NewCategoryRepository(db)
	categoryService := services.NewCategoryService(categoryRepository)
	categoryHandler := handlers.NewCategoryHandler(categoryService)

	orderRepository := repositories.NewOrderRepository(db)
	orderService := services.NewOrderService(orderRepository)
	orderHandler := handlers.NewOrderHandler(orderService)

	order_productRepository := repositories.NewOrderProductRepository(db)
	order_productService := services.NewOrderProductService(order_productRepository)
	order_productHandler := handlers.NewOrderProductHandler(order_productService)

	productRepository := repositories.NewProductRepository(db)
	productService := services.NewProductService(productRepository)
	productHandler := handlers.NewProductHandler(productService)

	product_imageRepository := repositories.NewProductImageRepository(db)
	product_imageService := services.NewProductImageService(product_imageRepository)
	product_imageHandler := handlers.NewProductImageHandler(product_imageService)

	reviewRepository := repositories.NewReviewRepository(db)
	reviewService := services.NewReviewService(reviewRepository)
	reviewHandler := handlers.NewReviewHandler(reviewService)

	shipingRepository := repositories.NewShipingRepository(db)
	shipingService := services.NewShipingService(shipingRepository)
	shipingHandler := handlers.NewShipingHandler(shipingService)

	userRepository := repositories.NewUserRepository(db)
	userService := services.NewUserService(userRepository)
	userHandler := handlers.NewUserHandler(userService)

	wishRepository := repositories.NewWishRepository(db)
	wishService := services.NewWishService(wishRepository)
	wishHandler := handlers.NewWishHandler(wishService)

	handler = &Handlers{
		CartHandler:         cartHandler,
		CategoryHandler:     categoryHandler,
		OrderHandler:        orderHandler,
		OrderProductHandler: order_productHandler,
		ProductHandler:      productHandler,
		ProductImageHandler: product_imageHandler,
		ReviewHandler:       reviewHandler,
		ShipingHandler:      shipingHandler,
		UserHandler:         userHandler,
		WishHandler:         wishHandler,
	}

}

func GetHandlers() *Handlers {
	return handler
}
