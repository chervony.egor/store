package handlers

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"store/app/models"
	"store/app/services"
	"strconv"
)

type ProductImageHandler struct {
	ProductImageService *services.ProductImageService
}

func NewProductImageHandler(ProductImageService *services.ProductImageService) *ProductImageHandler {
	return &ProductImageHandler{ProductImageService: ProductImageService}
}

// @Summary Create a new product_image
// @Tags ProductImage
// @Accept multipart/form-data
// @Description Create a new product_image
// @Param id query int true "product id for which you are uploading a photo"
// @Param image formData file true "The image file to upload"
// @Router /newProductImage [post]
func (h *ProductImageHandler) CreateProductImage(c echo.Context) error {

	image, err := c.FormFile("image")
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to upload image")
	}
	file, err := image.Open()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to check image")
	}
	defer file.Close()
	uuid, err := uuid.NewRandom()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to upload image")
	}
	if _, err := os.Stat("uploads"); os.IsNotExist(err) {
		err := os.Mkdir("uploads", 0755)
		if err != nil {
			log.Println(err)
			return c.JSON(http.StatusInternalServerError, "Failed to create 'uploads' folder")
		}
	}
	ext := filepath.Ext(image.Filename)
	fileName := "uploads/" + uuid.String() + ext
	out, err := os.Create(fileName)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to upload image")
	}
	defer out.Close()
	_, err = io.Copy(out, file)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to upload image")
	}

	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	product_image := &models.ProductImage{
		ProductId: id,
		ImagePath: fileName,
	}
	err = h.ProductImageService.CreateProductImage(product_image)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create product image")
	}

	return c.JSON(http.StatusOK, product_image)
}

// @Summary Get product_image by id
// @Tags ProductImage
// @Description Get product_image by id
// @Param id query int true "Id of product_image"
// @Router /getProductImagesById [get]
func (h *ProductImageHandler) GetProductImageByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	product_image, err := h.ProductImageService.GetProductImageByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get product image")
	}

	return c.JSON(http.StatusOK, product_image)
}

// @Summary Get all product_images
// @Tags ProductImage
// @Description Get all product_images
// @Router /allProductImages [get]
func (h *ProductImageHandler) GetAllProductImages(c echo.Context) error {
	product_images, err := h.ProductImageService.GetAllProductImages()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all product imagees")
	}
	return c.JSON(http.StatusOK, product_images)
}

// @Summary Update product_images
// @Security Authorization
// @Tags ProductImage
// @Description Update product_images
// @Param ProductImage body models.ProductImage true "ProductImage object to be updated"
// @Router /updateProductImage [put]
func (h *ProductImageHandler) UpdateProductImage(c echo.Context) error {
	product_image := &models.ProductImage{}
	if err := c.Bind(product_image); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.ProductImageService.UpdateProductImage(product_image)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update product image")
	}
	return c.JSON(http.StatusOK, "ProductImage updated")
}

// @Summary Delete product_image
// @Security Authorization
// @Tags ProductImage
// @Description Delete product_image
// @Param id query int true "Id of product_image to be deleted"
// @Router /deleteProductImage [delete]
func (h *ProductImageHandler) DeleteProductImage(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.ProductImageService.DeleteProductImage(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete product image")
	}
	return c.JSON(http.StatusOK, "ProductImage deleted")
}
