package handlers

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"store/app/models"
	"store/app/services"
	"strconv"
)

type OrderProductHandler struct {
	OrderProductService *services.OrderProductService
}

func NewOrderProductHandler(OrderProductService *services.OrderProductService) *OrderProductHandler {
	return &OrderProductHandler{OrderProductService: OrderProductService}
}

// @Summary Create a new order_product
// @Tags OrderProduct
// @Description Create a new order_product
// @Param OrderProduct body swagger_structs.CreateOrderProduct true "OrderProduct object to be created"
// @Router /newOrderProduct [post]
func (h *OrderProductHandler) CreateOrderProduct(c echo.Context) error {
	order_product := &models.OrderProduct{}
	if err := c.Bind(order_product); err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.OrderProductService.CreateOrderProduct(order_product)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to create order_product")
	}

	return c.JSON(http.StatusOK, order_product)
}

// @Summary Get order_product by id
// @Tags OrderProduct
// @Description Get order_product by id
// @Param id query int true "Id of order_product"
// @Router /getOrderProductById [get]
func (h *OrderProductHandler) GetOrderProductByID(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	order_product, err := h.OrderProductService.GetOrderProductByID(id)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get order_product")
	}

	return c.JSON(http.StatusOK, order_product)
}

// @Summary Get all order_product
// @Tags OrderProduct
// @Description Get all order_product
// @Router /allOrderProducts [get]
func (h *OrderProductHandler) GetAllOrderProducts(c echo.Context) error {
	order_products, err := h.OrderProductService.GetAllOrderProducts()
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to get all order_products")
	}
	return c.JSON(http.StatusOK, order_products)
}

// @Summary Update order_product
// @Security Authorization
// @Tags OrderProduct
// @Description Update order_product
// @Param OrderProduct body models.OrderProduct true "OrderProduct object to be updated"
// @Router /updateOrderProduct [put]
func (h *OrderProductHandler) UpdateOrderProduct(c echo.Context) error {
	order_product := &models.OrderProduct{}
	if err := c.Bind(order_product); err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err := h.OrderProductService.UpdateOrderProduct(order_product)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to update order_product")
	}
	return c.JSON(http.StatusOK, "OrderProduct updated")
}

// @Summary Delete order_product
// @Security Authorization
// @Tags OrderProduct
// @Description Delete order_product
// @Param id query int true "Id of order_product to be deleted"
// @Router /deleteOrderProduct [delete]
func (h *OrderProductHandler) DeleteOrderProduct(c echo.Context) error {
	param := c.QueryParam("id")
	id, err := strconv.Atoi(param)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Invalid request")
	}
	err = h.OrderProductService.DeleteOrderProduct(id)
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, "Failed to delete order_product")
	}
	return c.JSON(http.StatusOK, "OrderProduct deleted")
}
