package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
)

type IShipingRepository interface {
	Create(user *models.Shiping) error
	GetAll() ([]*models.Shiping, error)
	GetByID(id int) (*models.Shiping, error)
	Update(user *models.Shiping) error
	Delete(id int) error
}

type ShipingRepository struct {
	db *sqlx.DB
}

func NewShipingRepository(db *sqlx.DB) *ShipingRepository {
	return &ShipingRepository{db: db}
}

func (r *ShipingRepository) Create(shiping *models.Shiping) error {
	_, err := r.db.NamedExec("INSERT INTO shipings(user_id, email, first_name, last_name, country, city, street, zip, phone) VALUES (:user_id, :email, :first_name, :last_name, :country, :city, :street, :zip, :phone)", shiping)
	if err != nil {
		return err
	}
	return nil
}

func (r *ShipingRepository) GetAll() ([]*models.Shiping, error) {
	var shipings []*models.Shiping
	err := r.db.Select(&shipings, "SELECT * FROM shipings")
	return shipings, err
}

func (r *ShipingRepository) GetByID(id int) (*models.Shiping, error) {
	shiping := models.Shiping{}
	err := r.db.Get(&shiping, "SELECT * FROM shipings WHERE id = $1", id)
	return &shiping, err
}

func (r *ShipingRepository) Update(shiping *models.Shiping) error {
	_, err := r.db.NamedExec("UPDATE shipings SET user_id=:user_id, email=:email, first_name=:first_name, last_name=:last_name, country=:country, city=:city, street=:street, zip=:zip, phone=:phone WHERE id=:id", shiping)
	return err
}

func (r *ShipingRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM shipings WHERE id = $1", id)
	return err
}
