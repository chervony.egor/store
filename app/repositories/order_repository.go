package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
)

type IOrderRepository interface {
	Create(user *models.Order) error
	GetAll() ([]*models.Order, error)
	GetByID(id int) (*models.Order, error)
	Update(user *models.Order) error
	Delete(id int) error
}

type OrderRepository struct {
	db *sqlx.DB
}

func NewOrderRepository(db *sqlx.DB) *OrderRepository {
	return &OrderRepository{db: db}
}

func (r *OrderRepository) Create(order *models.Order) error {
	_, err := r.db.NamedExec("INSERT INTO orders(shipping_id, user_id, date) VALUES (:shipping_id, :user_id, :date)", order)
	if err != nil {
		return err
	}
	return nil
}

func (r *OrderRepository) GetAll() ([]*models.Order, error) {
	var orders []*models.Order
	err := r.db.Select(&orders, "SELECT * FROM orders")
	return orders, err
}

func (r *OrderRepository) GetByID(id int) (*models.Order, error) {
	order := models.Order{}
	err := r.db.Get(&order, "SELECT * FROM orders WHERE id = $1", id)
	return &order, err
}

func (r *OrderRepository) Update(order *models.Order) error {
	_, err := r.db.NamedExec("UPDATE orders SET shipping_id=:shipping_id, user_id=:user_id, date=:date WHERE id=:id", order)
	return err
}

func (r *OrderRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM orders WHERE id = $1", id)
	return err
}
