package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
)

type IOrderProductRepository interface {
	Create(user *models.OrderProduct) error
	GetAll() ([]*models.OrderProduct, error)
	GetByID(id int) (*models.OrderProduct, error)
	Update(user *models.OrderProduct) error
	Delete(id int) error
}

type OrderProductRepository struct {
	db *sqlx.DB
}

func NewOrderProductRepository(db *sqlx.DB) *OrderProductRepository {
	return &OrderProductRepository{db: db}
}

func (r *OrderProductRepository) Create(order_product *models.OrderProduct) error {
	_, err := r.db.NamedExec("INSERT INTO order_products(order_id, product_id, quantity) VALUES (:order_id, :product_id, :quantity)", order_product)
	if err != nil {
		return err
	}
	return nil
}

func (r *OrderProductRepository) GetAll() ([]*models.OrderProduct, error) {
	var order_products []*models.OrderProduct
	err := r.db.Select(&order_products, "SELECT * FROM order_products")
	return order_products, err
}

func (r *OrderProductRepository) GetByID(id int) (*models.OrderProduct, error) {
	order_product := models.OrderProduct{}
	err := r.db.Get(&order_product, "SELECT * FROM order_products WHERE id = $1", id)
	return &order_product, err
}

func (r *OrderProductRepository) Update(order_product *models.OrderProduct) error {
	_, err := r.db.NamedExec("UPDATE order_products SET order_id=:order_id, product_id=:product_id, quantity=:quantity WHERE id=:id", order_product)
	return err
}

func (r *OrderProductRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM order_products WHERE id = $1", id)
	return err
}
