package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
)

type ICategoryRepository interface {
	Create(user *models.Category) error
	GetAll() ([]*models.Category, error)
	GetByID(id int) (*models.Category, error)
	Update(user *models.Category) error
	Delete(id int) error
}

type CategoryRepository struct {
	db *sqlx.DB
}

func NewCategoryRepository(db *sqlx.DB) *CategoryRepository {
	return &CategoryRepository{db: db}
}

func (r *CategoryRepository) Create(category *models.Category) error {
	_, err := r.db.NamedExec("INSERT INTO categories(name) VALUES (:name)", category)
	if err != nil {
		return err
	}
	return nil
}

func (r *CategoryRepository) GetAll() ([]*models.Category, error) {
	var categorys []*models.Category
	err := r.db.Select(&categorys, "SELECT * FROM categories")
	return categorys, err
}

func (r *CategoryRepository) GetByID(id int) (*models.Category, error) {
	category := models.Category{}
	err := r.db.Get(&category, "SELECT * FROM categories WHERE id = $1", id)
	return &category, err
}

func (r *CategoryRepository) Update(category *models.Category) error {
	_, err := r.db.NamedExec("UPDATE categories SET name=:name WHERE id=:id", category)
	return err
}

func (r *CategoryRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM categories WHERE id = $1", id)
	return err
}
