package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
	"store/extensions/jwt"
	"store/extensions/security"
)

type IUserRepository interface {
	Create(user *models.User) error
	GetAll() ([]*models.User, error)
	GetByID(id int) (*models.User, error)
	Update(user *models.User) error
	Delete(id int) error
	Login(user *models.User) (string, error)
	GetByEmail(email string) (*models.User, error)
	UpdatePassword(id int, password string) error
}

type UserRepository struct {
	db *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{db: db}
}

func (r *UserRepository) Create(user *models.User) error {
	hashedPassword, _ := security.Hash(user.Password)
	user.Password = string(hashedPassword)
	_, err := r.db.NamedExec("INSERT INTO users(email, password) VALUES (:email, :password)", user)
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepository) GetAll() ([]*models.User, error) {
	var users []*models.User
	err := r.db.Select(&users, "SELECT * FROM users")
	return users, err
}

func (r *UserRepository) GetByID(id int) (*models.User, error) {
	user := models.User{}
	err := r.db.Get(&user, "SELECT * FROM users WHERE id = $1", id)
	return &user, err
}

func (r *UserRepository) Update(user *models.User) error {
	hashedPassword, _ := security.Hash(user.Password)
	user.Password = string(hashedPassword)
	_, err := r.db.NamedExec("UPDATE users SET email=:email, password=:password WHERE id=:id", user)
	return err
}

func (r *UserRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM users WHERE id = $1", id)
	return err
}

func (r *UserRepository) GetByEmail(email string) (*models.User, error) {
	user := models.User{}
	err := r.db.Get(&user, "SELECT * FROM users WHERE email = $1", email)
	return &user, err
}

func (r *UserRepository) UpdatePassword(id int, password string) error {
	user, err := r.GetByID(id)
	if err != nil {
		return err
	}
	user.Password = password
	err = r.Update(user)
	return err
}

func (r *UserRepository) Login(user *models.User) (string, error) {
	dbUser := models.User{}
	err := r.db.Get(&dbUser, "SELECT * FROM users WHERE email = $1", user.Email)
	if err != nil {
		return "", err
	}
	err = security.VerifyPassword(dbUser.Password, user.Password)
	if err != nil {
		return "", err
	}
	token, err := jwt.GenerateToken(&dbUser)
	if err != nil {
		return "", err
	}
	return token, nil
}
