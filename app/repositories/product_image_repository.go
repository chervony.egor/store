package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
)

type IProductImageRepository interface {
	Create(user *models.ProductImage) error
	GetAll() ([]*models.ProductImage, error)
	GetByID(id int) (*models.ProductImage, error)
	Update(user *models.ProductImage) error
	Delete(id int) error
}

type ProductImageRepository struct {
	db *sqlx.DB
}

func NewProductImageRepository(db *sqlx.DB) *ProductImageRepository {
	return &ProductImageRepository{db: db}
}

func (r *ProductImageRepository) Create(product_image *models.ProductImage) error {
	_, err := r.db.NamedExec("INSERT INTO product_images(product_id, image_path) VALUES (:product_id, :image_path)", product_image)
	if err != nil {
		return err
	}
	return nil
}

func (r *ProductImageRepository) GetAll() ([]*models.ProductImage, error) {
	var product_images []*models.ProductImage
	err := r.db.Select(&product_images, "SELECT * FROM product_images")
	return product_images, err
}

func (r *ProductImageRepository) GetByID(id int) (*models.ProductImage, error) {
	product_image := models.ProductImage{}
	err := r.db.Get(&product_image, "SELECT * FROM product_images WHERE id = $1", id)
	return &product_image, err
}

func (r *ProductImageRepository) Update(product_image *models.ProductImage) error {
	_, err := r.db.NamedExec("UPDATE product_images SET product_id=:product_id, image_path=:image_path WHERE id=:id", product_image)
	return err
}

func (r *ProductImageRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM product_images WHERE id = $1", id)
	return err
}
