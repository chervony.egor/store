package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
)

type IWishRepository interface {
	Create(user *models.Wish) error
	GetAll() ([]*models.Wish, error)
	GetByID(id int) (*models.Wish, error)
	Update(user *models.Wish) error
	Delete(id int) error
	GetByUserId(id int) ([]*models.Wish, error)
}

type WishRepository struct {
	db *sqlx.DB
}

func NewWishRepository(db *sqlx.DB) *WishRepository {
	return &WishRepository{db: db}
}

func (r *WishRepository) Create(wish *models.Wish) error {
	_, err := r.db.NamedExec("INSERT INTO wishes(user_id, product_id) VALUES (:user_id, :product_id)", wish)
	if err != nil {
		return err
	}
	return nil
}

func (r *WishRepository) GetAll() ([]*models.Wish, error) {
	var wishs []*models.Wish
	err := r.db.Select(&wishs, "SELECT * FROM wishes")
	return wishs, err
}

func (r *WishRepository) GetByID(id int) (*models.Wish, error) {
	wish := models.Wish{}
	err := r.db.Get(&wish, "SELECT * FROM wishes WHERE id = $1", id)
	return &wish, err
}

func (r *WishRepository) Update(wish *models.Wish) error {
	_, err := r.db.NamedExec("UPDATE wishes SET user_id=:user_id, product_id=:product_id WHERE id=:id", wish)
	return err
}

func (r *WishRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM wishes WHERE id = $1", id)
	return err
}

func (r *WishRepository) GetByUserId(id int) ([]*models.Wish, error) {
	var wishs []*models.Wish
	err := r.db.Select(&wishs, "SELECT * FROM wishes WHERE user_id = $1", id)
	return wishs, err
}
