package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
)

type ICartRepository interface {
	Create(user *models.Cart) error
	GetAll() ([]*models.Cart, error)
	GetByID(id int) (*models.Cart, error)
	Update(user *models.Cart) error
	Delete(id int) error
	GetByUserId(id int) ([]*models.Cart, error)
}

type CartRepository struct {
	db *sqlx.DB
}

func NewCartRepository(db *sqlx.DB) *CartRepository {
	return &CartRepository{db: db}
}

func (r *CartRepository) Create(cart *models.Cart) error {
	_, err := r.db.NamedExec("INSERT INTO carts(user_id, product_id, quantity) VALUES (:user_id, :product_id, :quantity)", cart)
	if err != nil {
		return err
	}
	return nil
}

func (r *CartRepository) GetAll() ([]*models.Cart, error) {
	var carts []*models.Cart
	err := r.db.Select(&carts, "SELECT * FROM carts")
	return carts, err
}

func (r *CartRepository) GetByID(id int) (*models.Cart, error) {
	cart := models.Cart{}
	err := r.db.Get(&cart, "SELECT * FROM carts WHERE id = $1", id)
	return &cart, err
}

func (r *CartRepository) Update(cart *models.Cart) error {
	_, err := r.db.NamedExec("UPDATE carts SET user_id=:user_id, product_id=:product_id, quantity=:quantity WHERE id=:id", cart)
	return err
}

func (r *CartRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM carts WHERE id = $1", id)
	return err
}

func (r *CartRepository) GetByUserId(id int) ([]*models.Cart, error) {
	var carts []*models.Cart
	err := r.db.Select(&carts, "SELECT * FROM carts WHERE user_id = $1", id)
	return carts, err
}
