package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
)

type IReviewRepository interface {
	Create(user *models.Review) error
	GetAll() ([]*models.Review, error)
	GetByID(id int) (*models.Review, error)
	Update(user *models.Review) error
	Delete(id int) error
	GetByProductId(id int) ([]*models.Review, error)
}

type ReviewRepository struct {
	db *sqlx.DB
}

func NewReviewRepository(db *sqlx.DB) *ReviewRepository {
	return &ReviewRepository{db: db}
}

func (r *ReviewRepository) Create(review *models.Review) error {
	_, err := r.db.NamedExec("INSERT INTO reviews(user_id, product_id, rate, message, date) VALUES (:user_id, :product_id, :rate, :message, :date)", review)
	if err != nil {
		return err
	}

	_, err = r.db.Exec("UPDATE products SET avg_rate = (SELECT AVG(rate) FROM reviews WHERE product_id = $1), reviews = (SELECT COUNT(*) FROM reviews WHERE product_id = $1) WHERE id = $1", review.ProductId)
	if err != nil {
		return err
	}

	return nil
}

func (r *ReviewRepository) GetAll() ([]*models.Review, error) {
	var reviews []*models.Review
	err := r.db.Select(&reviews, "SELECT * FROM reviews")
	return reviews, err
}

func (r *ReviewRepository) GetByID(id int) (*models.Review, error) {
	review := models.Review{}
	err := r.db.Get(&review, "SELECT * FROM reviews WHERE id = $1", id)
	return &review, err
}

func (r *ReviewRepository) Update(review *models.Review) error {
	_, err := r.db.NamedExec("UPDATE reviews SET user_id=:user_id, product_id=:product_id, rate=:rate, message=:message, date=:date WHERE id=:id", review)
	return err
}

func (r *ReviewRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM reviews WHERE id = $1", id)
	return err
}

func (r *ReviewRepository) GetByProductId(id int) ([]*models.Review, error) {
	var reviews []*models.Review
	err := r.db.Select(&reviews, "SELECT * FROM reviews WHERE product_id = $1", id)
	return reviews, err
}
