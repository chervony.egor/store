package repositories

import (
	"github.com/jmoiron/sqlx"
	"store/app/models"
)

type IProductRepository interface {
	Create(user *models.Product) error
	GetAll() ([]*models.Product, error)
	GetByID(id int) (*models.Product, error)
	Update(user *models.Product) error
	Delete(id int) error
	BestSelling() (*models.Product, error)
	GetByCategoryId(id int) ([]*models.Product, error)
	GetByCategoryName(name string) ([]*models.Product, error)
	BestSellingByCategoryId(id int) (*models.Product, error)
}

type ProductRepository struct {
	db *sqlx.DB
}

func NewProductRepository(db *sqlx.DB) *ProductRepository {
	return &ProductRepository{db: db}
}

func (r *ProductRepository) Create(product *models.Product) error {
	_, err := r.db.NamedExec("INSERT INTO products(brand, model, category_id, description, price, sales, avg_rate, reviews, quantity) VALUES (:brand, :model, :category_id, :description, :price, :sales, :avg_rate, :reviews, :quantity)", product)
	if err != nil {
		return err
	}
	return nil
}

func (r *ProductRepository) GetAll() ([]*models.Product, error) {
	var products []*models.Product
	err := r.db.Select(&products, "SELECT * FROM products")
	return products, err
}

func (r *ProductRepository) GetByID(id int) (*models.Product, error) {
	product := models.Product{}
	err := r.db.Get(&product, "SELECT * FROM products WHERE id = $1", id)
	return &product, err
}

func (r *ProductRepository) Update(product *models.Product) error {
	_, err := r.db.NamedExec("UPDATE products SET brand=:brand, model=:model, category_id=:category_id, description=:description, price=:price, sales=:sales, avg_rate=:avg_rate, reviews=:reviews, quantity=:quantity WHERE id=:id", product)
	return err
}

func (r *ProductRepository) Delete(id int) error {
	_, err := r.db.Exec("DELETE FROM products WHERE id = $1", id)
	return err
}

func (r *ProductRepository) BestSelling() (*models.Product, error) {
	products := []*models.Product{}
	err := r.db.Select(&products, "SELECT * FROM products ORDER BY sales DESC LIMIT 1")
	return products[0], err
}
func (r *ProductRepository) GetByCategoryId(id int) ([]*models.Product, error) {
	var products []*models.Product
	err := r.db.Select(&products, "SELECT * FROM products WHERE category_id = $1", id)
	return products, err
}

func (r *ProductRepository) GetByCategoryName(name string) ([]*models.Product, error) {
	var products []*models.Product
	err := r.db.Select(&products, "SELECT * FROM products WHERE category_id IN (SELECT id FROM categories WHERE name = $1)", name)
	return products, err
}

func (r *ProductRepository) BestSellingByCategoryId(id int) (*models.Product, error) {
	products := []*models.Product{}
	err := r.db.Select(&products, "SELECT * FROM products WHERE category_id = $1 ORDER BY sales DESC LIMIT 1", id)
	return products[0], err
}
