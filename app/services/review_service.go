package services

import (
	"store/app/models"
	"store/app/repositories"
)

type IReviewService interface {
	CreateReview(user *models.Review) error
	GetReviewByID(id int) (*models.Review, error)
	GetAllReviews() ([]*models.Review, error)
	UpdateReview(user *models.Review) error
	DeleteReview(id int) error
	GetByProductId(id int) ([]*models.Review, error)
}

type ReviewService struct {
	ReviewRepository *repositories.ReviewRepository
}

func NewReviewService(ReviewRepository *repositories.ReviewRepository) *ReviewService {
	return &ReviewService{ReviewRepository: ReviewRepository}
}

func (s *ReviewService) CreateReview(review *models.Review) error {
	err := s.ReviewRepository.Create(review)
	if err != nil {
		return err
	}
	return nil
}
func (s *ReviewService) GetReviewByID(id int) (*models.Review, error) {
	review, err := s.ReviewRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return review, nil
}
func (s *ReviewService) GetAllReviews() ([]*models.Review, error) {
	reviews, err := s.ReviewRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return reviews, nil

}
func (s *ReviewService) UpdateReview(review *models.Review) error {
	err := s.ReviewRepository.Update(review)
	if err != nil {
		return err
	}
	return nil
}
func (s *ReviewService) DeleteReview(id int) error {
	err := s.ReviewRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}

func (s *ReviewService) GetByProductId(id int) ([]*models.Review, error) {
	reviews, err := s.ReviewRepository.GetByProductId(id)
	if err != nil {
		return nil, err
	}
	return reviews, nil
}
