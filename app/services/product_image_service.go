package services

import (
	"store/app/models"
	"store/app/repositories"
)

type IProductImageService interface {
	CreateProductImage(user *models.ProductImage) error
	GetProductImageByID(id int) (*models.ProductImage, error)
	GetAllProductImages() ([]*models.ProductImage, error)
	UpdateProductImage(user *models.ProductImage) error
	DeleteProductImage(id int) error
}

type ProductImageService struct {
	ProductImageRepository *repositories.ProductImageRepository
}

func NewProductImageService(ProductImageRepository *repositories.ProductImageRepository) *ProductImageService {
	return &ProductImageService{ProductImageRepository: ProductImageRepository}
}

func (s *ProductImageService) CreateProductImage(product_image *models.ProductImage) error {
	err := s.ProductImageRepository.Create(product_image)
	if err != nil {
		return err
	}
	return nil
}
func (s *ProductImageService) GetProductImageByID(id int) (*models.ProductImage, error) {
	product_image, err := s.ProductImageRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return product_image, nil
}
func (s *ProductImageService) GetAllProductImages() ([]*models.ProductImage, error) {
	product_images, err := s.ProductImageRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return product_images, nil

}
func (s *ProductImageService) UpdateProductImage(product_image *models.ProductImage) error {
	err := s.ProductImageRepository.Update(product_image)
	if err != nil {
		return err
	}
	return nil
}
func (s *ProductImageService) DeleteProductImage(id int) error {
	err := s.ProductImageRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}
