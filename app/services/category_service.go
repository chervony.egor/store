package services

import (
	"store/app/models"
	"store/app/repositories"
)

type ICategoryService interface {
	CreateCategory(user *models.Category) error
	GetCategoryByID(id int) (*models.Category, error)
	GetAllCategories() ([]*models.Category, error)
	UpdateCategory(user *models.Category) error
	DeleteCategory(id int) error
}

type CategoryService struct {
	CategoryRepository *repositories.CategoryRepository
}

func NewCategoryService(CategoryRepository *repositories.CategoryRepository) *CategoryService {
	return &CategoryService{CategoryRepository: CategoryRepository}
}

func (s *CategoryService) CreateCategory(category *models.Category) error {
	err := s.CategoryRepository.Create(category)
	if err != nil {
		return err
	}
	return nil
}
func (s *CategoryService) GetCategoryByID(id int) (*models.Category, error) {
	category, err := s.CategoryRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return category, nil
}
func (s *CategoryService) GetAllCategories() ([]*models.Category, error) {
	categorys, err := s.CategoryRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return categorys, nil

}
func (s *CategoryService) UpdateCategory(category *models.Category) error {
	err := s.CategoryRepository.Update(category)
	if err != nil {
		return err
	}
	return nil
}
func (s *CategoryService) DeleteCategory(id int) error {
	err := s.CategoryRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}
