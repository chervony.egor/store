package services

import (
	"store/app/models"
	"store/app/repositories"
)

type ICartService interface {
	CreateCart(user *models.Cart) error
	GetCartByID(id int) (*models.Cart, error)
	GetAllCarts() ([]*models.Cart, error)
	UpdateCart(user *models.Cart) error
	DeleteCart(id int) error
	GetByUserId(id int) ([]*models.Cart, error)
}

type CartService struct {
	CartRepository *repositories.CartRepository
}

func NewCartService(CartRepository *repositories.CartRepository) *CartService {
	return &CartService{CartRepository: CartRepository}
}

func (s *CartService) CreateCart(cart *models.Cart) error {
	err := s.CartRepository.Create(cart)
	if err != nil {
		return err
	}
	return nil
}
func (s *CartService) GetCartByID(id int) (*models.Cart, error) {
	cart, err := s.CartRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return cart, nil
}
func (s *CartService) GetAllCarts() ([]*models.Cart, error) {
	carts, err := s.CartRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return carts, nil

}
func (s *CartService) UpdateCart(cart *models.Cart) error {
	err := s.CartRepository.Update(cart)
	if err != nil {
		return err
	}
	return nil
}
func (s *CartService) DeleteCart(id int) error {
	err := s.CartRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}

func (s *CartService) GetByUserId(id int) ([]*models.Cart, error) {
	users, err := s.CartRepository.GetByUserId(id)
	if err != nil {
		return nil, err
	}
	return users, nil
}
