package services

import (
	"store/app/models"
	"store/app/repositories"
)

type IOrderService interface {
	CreateOrder(user *models.Order) error
	GetOrderByID(id int) (*models.Order, error)
	GetAllOrders() ([]*models.Order, error)
	UpdateOrder(user *models.Order) error
	DeleteOrder(id int) error
}

type OrderService struct {
	OrderRepository *repositories.OrderRepository
}

func NewOrderService(OrderRepository *repositories.OrderRepository) *OrderService {
	return &OrderService{OrderRepository: OrderRepository}
}

func (s *OrderService) CreateOrder(order *models.Order) error {
	err := s.OrderRepository.Create(order)
	if err != nil {
		return err
	}
	return nil
}
func (s *OrderService) GetOrderByID(id int) (*models.Order, error) {
	order, err := s.OrderRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return order, nil
}
func (s *OrderService) GetAllOrders() ([]*models.Order, error) {
	orders, err := s.OrderRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return orders, nil

}
func (s *OrderService) UpdateOrder(order *models.Order) error {
	err := s.OrderRepository.Update(order)
	if err != nil {
		return err
	}
	return nil
}
func (s *OrderService) DeleteOrder(id int) error {
	err := s.OrderRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}
