package services

import (
	"store/app/models"
	"store/app/repositories"
)

type IUserService interface {
	CreateUser(user *models.User) error
	GetUserByID(id int) (*models.User, error)
	GetAllUsers() ([]*models.User, error)
	UpdateUser(user *models.User) error
	DeleteUser(id int) error
	Login(user *models.User) (string, error)
	GetByEmail(email string) (*models.User, error)
	UpdatePassword(id int, password string) error
}

type UserService struct {
	UserRepository *repositories.UserRepository
}

func NewUserService(UserRepository *repositories.UserRepository) *UserService {
	return &UserService{UserRepository: UserRepository}
}

func (s *UserService) CreateUser(user *models.User) error {
	err := s.UserRepository.Create(user)
	if err != nil {
		return err
	}
	return nil
}
func (s *UserService) GetUserByID(id int) (*models.User, error) {
	user, err := s.UserRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}
func (s *UserService) GetAllUsers() ([]*models.User, error) {
	users, err := s.UserRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return users, nil

}
func (s *UserService) UpdateUser(user *models.User) error {
	err := s.UserRepository.Update(user)
	if err != nil {
		return err
	}
	return nil
}
func (s *UserService) DeleteUser(id int) error {
	err := s.UserRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}

func (s *UserService) GetByEmail(email string) (*models.User, error) {
	user, err := s.UserRepository.GetByEmail(email)
	return user, err
}
func (s *UserService) UpdatePassword(id int, password string) error {
	err := s.UserRepository.UpdatePassword(id, password)
	return err
}

func (s *UserService) Login(user *models.User) (string, error) {
	token, err := s.UserRepository.Login(user)
	if err != nil {
		return "", err
	}
	return token, nil
}
