package services

import (
	"store/app/models"
	"store/app/repositories"
)

type IProductService interface {
	CreateProduct(user *models.Product) error
	GetProductByID(id int) (*models.Product, error)
	GetAllProducts() ([]*models.Product, error)
	UpdateProduct(user *models.Product) error
	DeleteProduct(id int) error
	BestSelling() (*models.Product, error)
	GetByCategoryId(id int) ([]*models.Product, error)
	GetByCategoryName(name string) ([]*models.Product, error)
	BestSellingByCategoryId(id int) (*models.Product, error)
}

type ProductService struct {
	ProductRepository *repositories.ProductRepository
}

func NewProductService(ProductRepository *repositories.ProductRepository) *ProductService {
	return &ProductService{ProductRepository: ProductRepository}
}

func (s *ProductService) CreateProduct(product *models.Product) error {
	err := s.ProductRepository.Create(product)
	if err != nil {
		return err
	}
	return nil
}
func (s *ProductService) GetProductByID(id int) (*models.Product, error) {
	product, err := s.ProductRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return product, nil
}
func (s *ProductService) GetAllProducts() ([]*models.Product, error) {
	products, err := s.ProductRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return products, nil

}
func (s *ProductService) UpdateProduct(product *models.Product) error {
	err := s.ProductRepository.Update(product)
	if err != nil {
		return err
	}
	return nil
}
func (s *ProductService) DeleteProduct(id int) error {
	err := s.ProductRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}
func (s *ProductService) BestSelling() (*models.Product, error) {
	product, err := s.ProductRepository.BestSelling()
	if err != nil {
		return nil, err
	}
	return product, nil
}
func (s *ProductService) GetByCategoryId(id int) ([]*models.Product, error) {
	products, err := s.ProductRepository.GetByCategoryId(id)
	if err != nil {
		return nil, err
	}
	return products, nil
}
func (s *ProductService) GetByCategoryName(name string) ([]*models.Product, error) {
	products, err := s.ProductRepository.GetByCategoryName(name)
	if err != nil {
		return nil, err
	}
	return products, nil
}
func (s *ProductService) BestSellingByCategoryId(id int) (*models.Product, error) {
	product, err := s.ProductRepository.BestSellingByCategoryId(id)
	if err != nil {
		return nil, err
	}
	return product, nil
}
