package services

import (
	"store/app/models"
	"store/app/repositories"
)

type IWishService interface {
	CreateWish(user *models.Wish) error
	GetWishByID(id int) (*models.Wish, error)
	GetAllWishs() ([]*models.Wish, error)
	UpdateWish(user *models.Wish) error
	DeleteWish(id int) error
	GetByUserId(id int) ([]*models.Wish, error)
}

type WishService struct {
	WishRepository *repositories.WishRepository
}

func NewWishService(WishRepository *repositories.WishRepository) *WishService {
	return &WishService{WishRepository: WishRepository}
}

func (s *WishService) CreateWish(wish *models.Wish) error {
	err := s.WishRepository.Create(wish)
	if err != nil {
		return err
	}
	return nil
}
func (s *WishService) GetWishByID(id int) (*models.Wish, error) {
	wish, err := s.WishRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return wish, nil
}
func (s *WishService) GetAllWishs() ([]*models.Wish, error) {
	wishs, err := s.WishRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return wishs, nil

}
func (s *WishService) UpdateWish(wish *models.Wish) error {
	err := s.WishRepository.Update(wish)
	if err != nil {
		return err
	}
	return nil
}
func (s *WishService) DeleteWish(id int) error {
	err := s.WishRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}
func (s *WishService) GetByUserId(id int) ([]*models.Wish, error) {
	wishes, err := s.WishRepository.GetByUserId(id)
	if err != nil {
		return nil, err
	}
	return wishes, nil
}
