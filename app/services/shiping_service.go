package services

import (
	"store/app/models"
	"store/app/repositories"
)

type IShipingService interface {
	CreateShiping(user *models.Shiping) error
	GetShipingByID(id int) (*models.Shiping, error)
	GetAllShipings() ([]*models.Shiping, error)
	UpdateShiping(user *models.Shiping) error
	DeleteShiping(id int) error
}

type ShipingService struct {
	ShipingRepository *repositories.ShipingRepository
}

func NewShipingService(ShipingRepository *repositories.ShipingRepository) *ShipingService {
	return &ShipingService{ShipingRepository: ShipingRepository}
}

func (s *ShipingService) CreateShiping(shiping *models.Shiping) error {
	err := s.ShipingRepository.Create(shiping)
	if err != nil {
		return err
	}
	return nil
}
func (s *ShipingService) GetShipingByID(id int) (*models.Shiping, error) {
	shiping, err := s.ShipingRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return shiping, nil
}
func (s *ShipingService) GetAllShipings() ([]*models.Shiping, error) {
	shipings, err := s.ShipingRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return shipings, nil

}
func (s *ShipingService) UpdateShiping(shiping *models.Shiping) error {
	err := s.ShipingRepository.Update(shiping)
	if err != nil {
		return err
	}
	return nil
}
func (s *ShipingService) DeleteShiping(id int) error {
	err := s.ShipingRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}
