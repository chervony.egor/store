package services

import (
	"store/app/models"
	"store/app/repositories"
)

type IOrderProductService interface {
	CreateOrderProduct(user *models.OrderProduct) error
	GetOrderProductByID(id int) (*models.OrderProduct, error)
	GetAllOrderProducts() ([]*models.OrderProduct, error)
	UpdateOrderProduct(user *models.OrderProduct) error
	DeleteOrderProduct(id int) error
}

type OrderProductService struct {
	OrderProductRepository *repositories.OrderProductRepository
}

func NewOrderProductService(OrderProductRepository *repositories.OrderProductRepository) *OrderProductService {
	return &OrderProductService{OrderProductRepository: OrderProductRepository}
}

func (s *OrderProductService) CreateOrderProduct(order_product *models.OrderProduct) error {
	err := s.OrderProductRepository.Create(order_product)
	if err != nil {
		return err
	}
	return nil
}
func (s *OrderProductService) GetOrderProductByID(id int) (*models.OrderProduct, error) {
	order_product, err := s.OrderProductRepository.GetByID(id)
	if err != nil {
		return nil, err
	}
	return order_product, nil
}
func (s *OrderProductService) GetAllOrderProducts() ([]*models.OrderProduct, error) {
	order_products, err := s.OrderProductRepository.GetAll()
	if err != nil {
		return nil, err
	}
	return order_products, nil

}
func (s *OrderProductService) UpdateOrderProduct(order_product *models.OrderProduct) error {
	err := s.OrderProductRepository.Update(order_product)
	if err != nil {
		return err
	}
	return nil
}
func (s *OrderProductService) DeleteOrderProduct(id int) error {
	err := s.OrderProductRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}
