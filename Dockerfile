FROM golang:latest as builder
WORKDIR /app
COPY . .
RUN go mod download
RUN go build -o main .

FROM golang:latest
WORKDIR /root/
RUN go install -tags postgres github.com/golang-migrate/migrate/v4/cmd/migrate@latest
COPY --from=builder /app/main .
COPY --from=builder /app/config.yaml .
COPY --from=builder /app/migrations ./migrations
CMD ["./main"]

