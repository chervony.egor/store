#!/bin/sh
./setup_env.sh
go install -tags postgres github.com/golang-migrate/migrate/v4/cmd/migrate@latest
migrate -path migrations -database postgres://$DATABASE_USER:$DATABASE_PASSWORD@$DATABASE_HOST:$DATABASE_PORT/$DATABASE_NAME?sslmode=disable up
./main