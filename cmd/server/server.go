package server

import (
	"github.com/labstack/echo/v4"
	"log"
	"os"
	"store/app/routes"
	"store/config"
	database "store/db"
)

func Start() {
	e := echo.New()
	e.Static("/uploads", "uploads")
	db := database.GetDb()

	if _, err := os.Stat(".env"); os.IsNotExist(err) {
		database.AutoMigrate()
		log.Println(".env file not found, using default config")
	} else {
		config.GenerateFromEnv()
	}

	routes.InitRoutes(e, db)
	cfg := config.LoadConfig()
	address := cfg.Server.Address + ":" + cfg.Server.Port
	err := e.Start(address)
	if err != nil {
		log.Printf("[SERVER] %s", err)
	}
}
